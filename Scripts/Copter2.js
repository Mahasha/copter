setupRaf();

var $ = jQuery;
var heli;

$(document).ready(mainGame);

function mainGame2() {
    var cw = 1200;
    var  ch = 650;

    var ctx = $("#MyCanvas")[0].getContext("2d");

    var heli1 = new Image();
    var heli2 = new Image();
    heli1.src = "Img/heli.png";
    heli2.src = "Img/heli1.png";
	heli = heli2;

    var Fps = 30;

	function flipHorizontally(heli2,x,y){
    // move to x + img's width
    ctx.translate(x+heli2.width,y);

    // scaleX by -1; this "trick" flips horizontally
    ctx.scale(-1,1);

    // draw the img
    // no need for x,y since we've already translated
    ctx.drawImage(heli2,0,0);

    // always clean up -- reset transformations to default
    ctx.setTransform(1,0,0,1,0,0);
}

    var PlayerTank = {
        x: cw / 2,
        y: ch - 14,
        w: 200,
        h: 100,
        Pos: 1,
        sp: 3 / 16,
        rotation: 0,
        lastShootTime: 0,
        shootRate: 300,
        draw: function () {
            ctx.save();
            ctx.translate(this.x, this.y);
            var angleInRadians = this.rotation * Math.PI / 180;
            ctx.rotate(angleInRadians);
            ctx.drawImage(heli,-this.w,-this.h, this.w , this.h );
			flipHorizontally(heli2,150,30);
            ctx.restore();

        },
        update: function (dt) {
            var moving = false;

            if (keydown.up) {
                moving = true;
                PlayerTank.rotation = 0;
				//alert("2nd Copter");
                PlayerTank.y -= PlayerTank.sp*dt;
                if (PlayerTank.y < 0) {
                    PlayerTank.y = PlayerTank.w/6;

                }
            }
            if (keydown.left) {
                moving = true;
                PlayerTank.rotation = -90;

                PlayerTank.x -= PlayerTank.sp*dt;
                if (PlayerTank.x < 0) {
                    PlayerTank.x = PlayerTank.w / 6 + 2;

                }
            }

            if (keydown.right) {
                moving = true;
                PlayerTank.rotation = 0;
                PlayerTank.x += PlayerTank.sp*dt;
                if (PlayerTank.x > cw - PlayerTank.w / 6) {
                    PlayerTank.x = cw - PlayerTank.w / 6;

                }
            }

            if (keydown.down) {
                moving = true;
                PlayerTank.rotation = 180;
                PlayerTank.y += PlayerTank.sp*dt;
                if (PlayerTank.y > ch - PlayerTank.h) {
                    PlayerTank.y = ch - PlayerTank.h;
                }
            }

            if (moving) {
                PlayerTank.Pos = 1 + (PlayerTank.Pos + 1) % 7;
            }

            if (keydown.space) {
                PlayerTank.shoot();
            }
        },

        shoot: function () {
            var audio = new Audio('Sounds/shot.mp3');
            audio.play();
            var now = Date.now();
            if (now - this.lastShootTime < this.shootRate) return;
            this.lastShootTime = now;
            Missiles.add (new playerMissile(
            this.x,
            this.y,
            5,
            this.rotation));
        }
    };

    var Missiles = {
        add : function (newMissile) {
               this._collection.push(newMissile)
        },
        draw: function () {
            var missileCollection = this._collection;
            for (var i = 0; i < missileCollection.length; i++) {
                missileCollection[i].draw();
            }
        },
        update: function (dt) {
            var missileCollection = this._collection;
            for (var i = 0; i < missileCollection.length; i++) {
                missileCollection[i].update(dt);
            }
        },
        _collection: [],
    };

    function playerMissile(x, y, radius, rot) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.rot = rot;
    }

    playerMissile.prototype.draw = function () {
        ctx.fillStyle = "Red";
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
        //this.h*2 specify that the image is in the row 2 of the tiled image
    };

    playerMissile.prototype.update = function (dt) {
        var speed = 6 / 16 ;
        switch (this.rot) {
            case 0:
                this.y -= speed * dt;
                break;
            case 90:
                this.x += speed * dt;
                break;
            case -90:
                this.x -= speed * dt;
                break;
            case 180:
                this.y += speed * dt;
                break;
        }
    };

    var lastTime = Date.now();
    function game() {
        // compute time
        var now = Date.now();
        var dt = now - lastTime ;
        requestAnimationFrame(game);
        if (dt < 14 ) return; // max is 60 fps
        if (dt > 200 ) dt = 16 ; // in case of tab out, consider just ONE frame elapsed.
        // real work here :
        Draw();
        Update(dt);
    };
    game();

    function Draw() {
        ctx.clearRect(0, 0, cw, ch);
        //
        PlayerTank.draw();
        Missiles.draw();
    }

    function Update(dt) {
        PlayerTank.update(dt);
        Missiles.update(dt);
    }
};

function setupRaf() {
    var w = window;
    window.requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame || w.oRequestAnimationFrame;
}
